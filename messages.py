# Language file (english)

# Words
mIMAGE = "image"
mMOUNT = "mount"
mCOMMAND = "command"
mARGUMENTS = "arguments"
mSIZE = "size"
mTYPE = "type"
mOPTION = "option"
mKEY = "key"
mERROR = "error"
mCREATED = "created"
mSUCCESS = "success"
mFAIL = "fail"
mUNKNOWN = "Unknown {0}"
mBAD = "Bad {0}"
mINVALID = "Invalid {0}"
mMISSING = "Missing {0}"

# Texts
mINFO = 'Welcome to the USB gadget CLI!'
mHELP = 'Type in "help" for a list of commands'
mPROMPT = "{0}>>> "
mHAS_BEEN = "{1} has been {0}"
mAS = '{0} as "{1}"'
mUNSUITABLE = '{0} is unsuitabe'

# Texts (errors)
mGET_HELP = 'Try using "help"'
mNO_SUCH = "No such {0}"
mLIMIT = "Can't enable more {0}s"
mALREADY = "This {0} is already enabled"

# Helpers
def get_unknown_error(error):
    return f"{mUNKNOWN.format(mERROR)}: {error.__class__.__name__}"
