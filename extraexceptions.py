class InvalidArgumentError(BaseException):
    pass

class MissingArgumentError(BaseException):
    pass

class BadArgumentError(BaseException):
    pass

class LimitError(BaseException):
    pass
