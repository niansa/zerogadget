from subprocess import call
from os.path import basename
import os, time
# Manual configuration
configfs = "/sys/kernel/config"
gadgetname = "g0"
confname = "c"
confnum = 1
storagefuncnum = 0
language = "0x407"
maxstorages = 5
# Automatic configuration:
otgdevice = os.listdir("/sys/class/udc")[0]
gadgetdir = f"{configfs}/usb_gadget/{gadgetname}"
confdir = f"configs/{confname}.{str(confnum)}"
storagefuncname = f"mass_storage.usb{str(storagefuncnum)}"
storagefuncdir = f"functions/{storagefuncname}"


call(["modprobe", "libcomposite"])
try: os.mkdir(gadgetdir)
except FileExistsError: pass
os.chdir(gadgetdir)


def _writeto(path, data):
    with open(path, mode="w") as f:
        return f.write(data)

def _readfrom(path):
    with open(path, mode="r") as f:
        return f.read()


def init():
    # Basic initialisation
    os.mkdir(f"strings/{language}")
    os.mkdir(confdir)
    #_writeto("idVendor", "0x0000")
    #_writeto("idProduct", "0x0000")
    _writeto(f"strings/{language}/serialnumber", "1234567890ABCEF")
    _writeto(f"strings/{language}/manufacturer", "Nils")
    _writeto(f"strings/{language}/product", "Cool USB gadget")
    # Storage initalisation
    os.mkdir(storagefuncdir)
    for storage_num in range(maxstorages - 1):
        storage_num += 1
        os.mkdir(f"{storagefuncdir}/lun.{str(storage_num)}")
    enable_storage()
    # Enable gadget
    _writeto("UDC", otgdevice)

def force_apply():
    try:
        _writeto("UDC", "")
        _writeto("UDC", otgdevice)
    except: pass

def remove(usbfunct):
    try: os.remove(f"{confdir}/{usbfunct}")
    except: pass
    os.rmdir(f"functions/{usbfunct}")

def enable_storage(reapply=True):
    os.symlink(storagefuncdir,
               f"{confdir}/{storagefuncname}")
    if reapply:
        force_apply()

def disable_storage(reapply=True):
    os.remove(f"{confdir}/{storagefuncname}")
    if reapply:
        force_apply()

def remove_storage(storage_num):
    disable_storage(reapply=False) # Disable storage first...
    try:
        _writeto(f"{storagefuncdir}/lun.{str(storage_num)}/file", "\n")
    finally:
        enable_storage(reapply=False) # Now make it available again
        force_apply() # Force appliance
        try: raise # Try raising
        except: pass

def create_storage(imagefile, is_cdrom=False, is_ro=False):
    # Disable storage first...
    disable_storage()
    try:
        # Redefine some variables to strings
        if is_cdrom == True:
            is_cdrom = "1"
        else:
            is_cdrom = "0"
        if is_ro == True:
            is_ro = "1"
        else:
            is_ro = "0"
        # Search for next free storage_num
        storage_num = 0
        searchfailed = False
        for filename in os.listdir(storagefuncdir):
            if storage_num > maxstorages - 1:
                searchfailed = True
                break
            elif _readfrom(f"{storagefuncdir}/lun.{str(storage_num)}/file") == "": # Find first unused lun
                break
            else:
                storage_num += 1
        # Check if the maximum amount of storages is reached
        if not searchfailed:
            # Create storage_num
            _writeto(f"{storagefuncdir}/lun.{str(storage_num)}/cdrom", is_cdrom)
            _writeto(f"{storagefuncdir}/lun.{str(storage_num)}/ro", is_ro)
            _writeto(f"{storagefuncdir}/lun.{str(storage_num)}/file", imagefile)
            _writeto(f"{storagefuncdir}/lun.{str(storage_num)}/removable", "0")
    finally:
        # Now make the storage available again
        enable_storage()
        # Try raising
        try: raise
        except: pass
        # Return storage number if defined
        if searchfailed: raise NotImplementedError
        elif storage_num: return storage_num
        elif storage_num == 0: return 0

#def create_acm():
#    if os.access("functions/acm.usb0", 0):
#        pass
#    os.mkdir("functions/acm.usb0")
#    time.sleep(0.5)
#    os.symlink("functions/acm.usb0", f"{confdir}/acm.usb0")
#    reload()
#    call(["systemctl", "start", "serial-getty@ttyGS0.service"])
