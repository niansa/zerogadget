from datastore import storage_class


class settings_class:
    def __init__(self, configfile):
        self.configstorage = storage_class(configfile)
        self.set = self.configstorage.set
        self.options = {"cdromemu": {"category": "compatibility", "keytype": "boolean", "default": True}, "readonly": {"category": "compatibility", "keytype": "boolean", "default": False}}

    def get(self, key):
        if key in self.configstorage.get():
            return self.configstorage.get(key)
        else:
            try:
                return self.options[key]["default"]
            except KeyError:
                if key in self.options:
                    return None
                else:
                    raise KeyError
