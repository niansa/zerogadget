#! /usr/bin/env python3
import os
from os.path import basename
from subprocess import call
from sys import exit
from string import ascii_letters, digits
from datastore import storage_class
from settings import settings_class
from extraexceptions import *
from messages import *
import usbgadgets as gadget
configfsdir = "/sys/kernel/config/usb_gadget/"
imagedir = "/media/images/public"
allowed_chars = ascii_letters + digits + "-_+%&.()"
tempstorage = storage_class("/tmp/storages.json")
permastorage = storage_class(imagedir+"/config.json")
settings = settings_class(imagedir+"/config.json")
FNULL = open("/dev/null", mode="w")


# Helper functions
def welcome_msg():
    print('\033[2J\033[H')
    print(mINFO)
    print(mHELP)

def prompt():
    return input(mPROMPT.format("images"))

def nodeadspaces(string):
    try:
        # Remove leading spaces
        while string.startswith(" "):
            string = string[1:]
        # Remove trailing spaces
        while string[-1] == " ":
            string = string[:-1]
        return string
    except IndexError:
        return ""

def string_to_bool(string):
    if string.lower() in ["1", "+", "true", "yes"]:
        return True
    elif string.lower() in ["0", "-", "false", "no"]:
        return False
    else:
        raise ValueError

def simplify(string):
    newstring = ""
    # Abort on some string values for security reasons
    if string == "." or string == ".." or string.startswith("._"):
        raise InvalidArgumentError
    # Remove leading/trailing spaces
    string = nodeadspaces(string)
    # Interate through the letters
    for letter in string:
        if not letter in allowed_chars:
            newstring += "-"
        else:
            newstring += letter
    # Return generated string
    return newstring

def get_errsuffix(error, defaultmsg=""):
    try:
        return ": "+str(error.args[0])
    except:
        return defaultmsg

def check_if_cdrom(location):
        if location[-4:] == ".img":
            return False             # This is a RAW image
        elif location[-4:] == ".iso":
            if settings.get("cdromemu"):    
                return True         # This is an ISO image
            else:
                return False        # This is not an ISO image
        else:
            return None             # This is something else

def check_if_ro(location):
    return check_if_cdrom(location) or settings.get("readonly")

def get_imagepath(name):
    # Return full file path
    return f"{imagedir}/{simplify(name)}"

def get_imagename(location):
    # Get filename
    filename = basename(location)
    # return image name
    return filename

def get_image(location):
    # Check file type
    is_cdrom = check_if_cdrom(location)
    # If file type is bad
    if is_cdrom == None:
        return False
    # Get filename
    filename = basename(location)
    # Return dict
    return {"name": get_imagename(filename), "path": f"{imagedir}/{filename}", "cdrom": is_cdrom}

def get_images():
    disk_list = []
    for file in os.listdir(imagedir):
        # Get image details
        image = get_image(file)
        # Check if image has a valid type
        if not image:
            continue
        # Append file to list
        disk_list.append(image)
    # Return it all
    return disk_list


# Commands
commands = ["help", "exit", "conf_cli", "conf_get", "conf_set", "list", "create", "rename", "delete", "forceapply", "enable", "disable"]

def cmd_help(args=None):
    message = ''
    for command in commands:
        message += f" - {command}\n"
    return [True, message]

def cmd_exit(args=None):
    exit()

def cmd_list(args=None):
    images = ""
    # Iterate through the images
    for image in get_images():
        # Check if image is enabled
        if image['name'] in tempstorage.get().keys():
            sidenote = f"(enabled: {str(tempstorage.get()[image['name']])})"
        else:
            sidenote = ""
        # Append to string
        images += f" - {image['name']} {sidenote}\n"
    return [True, images]

def cmd_create(args):
    # Handle arguments
    try:
        args = args.strip().split(" ", 1)
        m_size = args[0]
        name = nodeadspaces(args[1])
    except:
        raise MissingArgumentError
    # Check if m_size is a valid number
    try:
        int(m_size)
    except ValueError:
        return [False, mBAD.format(mSIZE)]
    # Make name end with .img if it does not
    if name[-4:] != ".img":
        name += ".img"
    # Get file path
    path = get_imagepath(name)
    # Write file
    call(["dd", "if=/dev/zero", f"of={path}", "bs=1M", f"count={m_size}"], stdout=FNULL)
    # Get name
    name = get_imagename(path)
    # Return success
    return [True, mHAS_BEEN.format(mAS.format(mCREATED, name), mIMAGE)]

def cmd_rename(names):
    # Handle arguments
    namessplit = names.split(",", 1)
    if not len(namessplit) == 2:
        raise MissingArgumentError
    oldname = simplify(nodeadspaces(namessplit[0]))
    newname = simplify(nodeadspaces(namessplit[1]))
    # Get image paths
    oldpath = get_imagepath(oldname)
    newpath = get_imagepath(newname)
    # Get image datails
    oldimage = get_image(oldname)
    # Return error if image is bad
    if not oldimage:
        return [False, mBAD.format(mIMAGE)]
    # Try to delete the image
    try:
        os.rename(oldpath, newpath)
    except FileNotFoundError:
        return [False, mNO_SUCH.format(mIMAGE)]
    else:
        return [True]

def cmd_delete(name):
    # Raise exception if name is empty
    if name == "":
        raise MissingArgumentError
    # Get image path
    path = get_imagepath(name)
    # Get image datails
    image = get_image(path)
    # Return error if image is bad
    if not image:
        return [False, mBAD.format(mIMAGE)]
    # Try to delete the image
    try:
        os.remove(path)
    except FileNotFoundError:
        return [False, mNO_SUCH.format(mIMAGE)]
    else:
        return [True]

def cmd_forceapply(args=None):
    # Reenable storage
    gadget.disable_storage()
    gadget.enable_storage()
    return [True]

def cmd_save(args=None):
    # Save current list of enabled images
    permastorage.set("currents", list(tempstorage.get().keys()))
    return [True]

def cmd_restore(args=None):
    bad_images = []
    # Get list of last enabled images
    try:
        last_currents = permastorage.get("currents")
    except KeyError:
        last_currents = []
    # Reenable them
    for name in last_currents:
        try:
            cmd_enable(name, savenow=False)
        except:
            bad_images.append(name)
    # Force apply and save
    cmd_forceapply()
    cmd_save()
    return [True]

def cmd_enable(args, savenow=True):
    # Get name and extraargs
    argssplit = args.split(" *")
    name = argssplit[0]
    options = argssplit[1:]
    # Raise exception if name is empty
    if name == "":
        raise MissingArgumentError
    # Simplify name
    name = simplify(name)
    # Check if file is already enabled
    if name in list(tempstorage.get().keys()):
        return [False, mALREADY.format(mIMAGE)]
    # Get file path
    path = get_imagepath(name)
    # Check file exists
    if not os.access(path, 0):
        return [False, mNO_SUCH.format(mIMAGE)]
    try:
        is_cdrom = not "!cdromemu" in options and check_if_cdrom(path) or "cdromemu" in options
        is_ro = not "!readonly" in options and check_if_ro(path) or "readonly" in options
        # Create new storage, replacing the old one
        storage_num = gadget.create_storage(path, is_cdrom=is_cdrom, is_ro=is_ro)
    except NotImplementedError:
        # Return error if maximum amount of storage is exceeded
        return [False, mLIMIT.format(mIMAGE)]
    # Assign the storage number to the image
    tempstorage.set(name, storage_num)
    # Save
    if savenow:
        cmd_save()
    return [True]

def cmd_disable(name, savenow=True):
    current_images = list(tempstorage.get().keys())
    # Get name
    if name != "": # If name was given
        name = simplify(name)
    else:
        raise MissingArgumentError
    # Get storage number of specified name
    try:
        storage_num = tempstorage.get(name)
    except KeyError:
        return [False, mNO_SUCH.format(mIMAGE)]
    # Remove the storage
    gadget.remove_storage(storage_num)
    tempstorage.delete(name)
    # Save
    if savenow:
        cmd_save()
    return [True]

def cmd_conf_set(args):
    # Handle arguments
    argssplit = args.split("=", 1)
    if not len(argssplit) == 2:
        raise MissingArgumentError
    # Parse arguments
    for arg in argssplit:
        # Remove leading/trailing spaces
        try:
            arg = nodeadspaces(arg)
        except IndexError:
            raise MissingArgumentError
        # Define key and value
        if not "key" in locals():
            key = arg
        elif not "value" in locals():
            value = arg
    # Check if the key is a valid setting
    if not key in settings.options.keys():
        raise BadArgumentError
    # Convert the argument into correct format
    if settings.options[key]["keytype"] == "boolean":
        try:
            value = string_to_bool(value)
        except ValueError:
            raise InvalidArgumentError("value != boolean")
    elif settings.options[key]["keytype"] == "number":
        try:
            value = int(value)
        except ValueError:
            raise InvalidArgumentError("value != number")
    else:
        pass
    settings.set(key, value)
    return [True]

def cmd_conf_get(key):
    # Check if key was passed
    if nodeadspaces(key) == "":
        raise MissingArgumentError
    # Return keys value if possible
    try:
        return [True, settings.get(key)]
    except KeyError:
        return [False, mNO_SUCH.format(mKEY)]

def cmd_conf_cli(args=None):
    return [False, "Not yet implemented"]


# Main
# Initalise USB gadget
try: gadget.init()
except: pass
try: gadget.init_storage()
except: pass
# Welcome message
welcome_msg()
# Loop if module was not imported
while __name__ == "__main__":
    # Wait for command
    try:
        rawcmd = prompt()
    except:
        print('\n')
        cmd_exit()
    # Do nothing if command is empty
    if nodeadspaces(rawcmd) == "":
        continue
    # Split command
    command = rawcmd.strip().split(" ", 1)
    # Check if command exists
    if not command[0] in commands:
        print(mNO_SUCH.format(mCOMMAND))
        continue
    # Default command arguments to ""
    if len(command) == 1:
        command.append("")
    # Run command
    try:
        result = eval(f"cmd_{command[0]}('{command[1]}')")
    except MissingArgumentError as error:
        result = [False, f"{mMISSING.format(mARGUMENTS)}{get_errsuffix(error)}"]
    except InvalidArgumentError as error:
        result = [False, f"{mINVALID.format(mARGUMENTS)}{get_errsuffix(error)}"]
    except BadArgumentError as error:
        result = [False, f"{mBAD.format(mARGUMENTS)}{get_errsuffix(error)}"]
    #except Exception as error:
    #    result = [False, get_unknown_error(error)]
    # Get resulting message
    if result[0] == True:
        if len(result) == 1:
            msg = mSUCCESS.title()
        else:
            msg = result[1]
    else:
        msg = f"{mFAIL.title()}: {result[1]}\nHint: {mGET_HELP}"
    # Return resulting message
    print(msg)
