import json, os


class storage_class:
    def __init__(self, jsonfile):
        self.jsonfile = jsonfile
        try:
            self.get()
        except:
            with open(jsonfile, mode="w") as f:
                json.dump({}, f)
    
    def set(self, key, value):
        # Read file
        with open(self.jsonfile, mode="r") as f:
            config = json.load(f)
        # Change value
        config[key] = value
        # Save data
        with open(self.jsonfile, mode="w") as f:
            config = json.dump(config, f)
    
    def delete(self, key):
        # Read file
        with open(self.jsonfile, mode="r") as f:
            config = json.load(f)
        # Remove key
        del config[key]
        # Save data
        with open(self.jsonfile, mode="w") as f:
            config = json.dump(config, f)
    
    def get(self, key=None):
        # Read file
        with open(self.jsonfile, mode="r") as f:
            config = json.load(f)
        # Return value
        if key:
            return config[key]
        else:
            return config
